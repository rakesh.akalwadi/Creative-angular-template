const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

//Connect to Database
mongoose.connect(config.database);

//On Connetion
mongoose.connection.on('connected', () => {
    console.log('Connected to Database: '+config.database);
});

//On error
mongoose.connection.on('error', (err) => {
    console.log('Error in Database: '+err);
});

const app = express();

const users = require('./routes/users');

//Port Number
const port = 3000;

//Cors Middleware
app.use(cors());

//Set static folder 
app.use(express.static(path.join(__dirname, 'public')));

//Body Parser Middleware
app.use(bodyParser.json());

//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users);

//Index Route
app.get('/', (req, res) => {
    res.send('Invalid Endpoint');
});

//Start Server
app.listen(port, () => {
    console.log('Magic happens on port '+port); 
});
